

import {Fragment, useState, useEffect} from 'react';
import { Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'


export default function Login() {

		const [email,checkEmail] = useState('');
		const [password,checkPassword] = useState('');


		const [isActive, setIsActive] = useState(false);


		

		useEffect(() => {

			if(email !== '' && password !== ''){

				setIsActive(true);
			}

			else {

				setIsActive(false)
			}





		},[email,password])
	

		function loginUser(e){

			e.preventDefault();

			checkEmail('')
			checkPassword('')


			Swal.fire({

			title: 'こんにちは美しい',
			icon: 'success',
			text: 'Welcome Stranger'

		})

		}





		return(
		<Fragment>
		<h1>Login</h1>
		<Form onSubmit ={(e) => loginUser(e)}>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => checkEmail(e.target.value)}
					required

				/>
				
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify your Password"
						value={password}
						onChange={e => checkPassword(e.target.value)}
						required

						/>	
					</Form.Group>
					
						
						{isActive ?
						<Button variant="dark" type="submit" id="submitBtn">Submit</Button>
						:
						<Button variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
						}
					</Form>
					</Fragment>







			)

	}